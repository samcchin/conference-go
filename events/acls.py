import json
import requests
from .keys import PEXELS_API_KEY, OPEN_WEATHER_API_KEY


def get_photo(city, state):
    url = 'https://api.pexels.com/v1/search'
    headers = {"Authorization": PEXELS_API_KEY}
    params = {"query": f'{city},{state}', "per_page": 1}
    picture = requests.get(url, params=params, headers=headers)
    content = json.loads(picture.content)

    return {"picture_url": content["photos"][0]["src"]["original"]}


def get_weather_data(city, state):
    coordinates_url = 'http://api.openweathermap.org/geo/1.0/direct?'
    parameters = {
        "q":f'{city}, {state}, US',
        "appid":OPEN_WEATHER_API_KEY,
    }
    response = requests.get(coordinates_url, params=parameters)
    location = json.loads(response.content)
    lat = location[0]['lat']
    lon = location[0]['lon']

    weather_url = 'https://api.openweathermap.org/data/2.5/weather?'
    weather_parameters = {
        'lat': lat,
        'lon': lon,
        'appid': OPEN_WEATHER_API_KEY,
        'units': 'imperial',
    }
    weather_response = requests.get(weather_url, params=weather_parameters)
    weather = json.loads(weather_response.content)

    return {
        "temp": weather['main']['temp'],
        "description": weather['weather'][0]['description']
    }


    # coordinates = requests.get(f'http://api.openweathermap.org/geo/1.0/direct?q={city},{state},{"US"}&limit={1}&appid={OPEN_WEATHER_API_KEY}')
    # coordinates_json = json.loads(coordinates.content)
    # latitude = coordinates_json[0]["lat"]
    # longitude = coordinates_json[0]["lon"]

    # weather_data = requests.get(f'https://api.openweathermap.org/data/2.5/weather?lat={latitude}&lon={longitude}&appid={OPEN_WEATHER_API_KEY}&units={"imperial"}')
    # data = json.loads(weather_data)
    # return {
    #     "temp": data['main']['temp'],
    #     "description": data['weather'][0]['description'],
    # }
